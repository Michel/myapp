module Core
  module User
    class View < HyperComponent

      render DIV do
        H1 { "User view"}
        PRE{ @result }

        BUTTON {"Call Core::User::SomeOp"}.on(:click) { # broken
          ::Core::User::SomeOp.run(value: 42).then {
            mutate @result = "Core - OK"
          }.fail(mutate @result = "Broken")
        }
        BUTTON {"Call Other::User::SomeOp"}.on(:click) { # broken
          ::Other::User::SomeOp.run(value: 42).then {
            mutate @result = "Other::User - OK"
          }.fail(mutate @result = "Broken")
        }
        BUTTON {"Call Other::Stuff::SomeOp"}.on(:click) { # working
          ::Other::Stuff::SomeOp.run(value: 42).then {
            mutate @result = "Other::Stuff - OK"
          }.fail(mutate @result = "Broken")
        }

        BUTTON {"Call SimpleOp"}.on(:click) { # working
          SimpleOp.run(value: 42).then {
            mutate @result = "Simple - OK"
          }.fail(mutate @result = "Broken")
        }
      end
    end
  end
end