module Other
  module User
    class SomeOp < ::Hyperstack::ControllerOp

      param :acting_user, nils: true
      param :value

      step { puts "OTHER::User yeah! #{params.value}"}
    end
  end
end