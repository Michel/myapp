module Other
  module Stuff
    class SomeOp < ::Hyperstack::ControllerOp

      param :acting_user, nils: true
      param :value

      step { puts "OTHER::Stuff yeah! #{params.value}"}
    end
  end
end