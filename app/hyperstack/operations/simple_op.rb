class SimpleOp < Hyperstack::ControllerOp

  param :acting_user, nils: true
  param :value

  step {puts "Simple #{params.value}"}
end