module Core
  module User
    class SomeOp < ::Hyperstack::ControllerOp

      param :acting_user, nils: true
      param :value

      step { puts "CORE yeah! #{params.value}"}
    end
  end
end